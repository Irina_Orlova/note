﻿using Note.Models;
namespace Note.Data.Interface
{
    /// <summary>
    /// Код компонента сервиса для работы с заметками
    /// </summary>
    public interface INoteService
    {
        #region Tag

        /// <summary>
        /// Получение id тэга
        /// </summary>
        /// <param name="id">Идентификатор</param>
        public Tags GetIdTag(int id);

        /// <summary>
        /// Полечение списка тэгов
        /// </summary>
        public List<Tags> GetDataTags();

        /// <summary>
        /// Добавление нового тэга
        /// </summary>
        /// <param name="objectTag">Модель тегов</param>
        public Tags CreateTag(Tags objectTag);

        /// <summary>
        /// Обновление существующего тэга
        /// </summary>
        /// <param name="objectTag">Модель тегов</param>
        public Tags UpdateTag(Tags objectTag);

        /// <summary>
        /// Удаление тега
        /// </summary>
        /// <param name="id">Идентификатор</param>
        void DeleteTag(int id);
        #endregion

        #region Note

        /// <summary>
        /// Полечение списка заметок
        /// </summary>
        /// <returns></returns>
        public List<Notes> GetDataNotes();

        /// <summary>
        /// Получение id заметки
        /// </summary>
        /// <param name="id">Идентификатор</param>
        public Notes GetIdNote(int id);

        /// <summary>
        /// Создание новой заметки
        /// </summary>
        /// <param name="objectNote">Модель заметок</param>
        /// <param name="selectedIdTags">Выбранный идентификатор тегов</param>
        public Notes CreateNote(Notes objectNote, int[] selectedIdTags);

        /// <summary>
        /// Обнавление существующей заметки
        /// </summary>
        /// <param name="objectNote">Модель заметок</param>
        /// <param name="selectedIdTags">Выбранный идентификатор тегов</param>
        public Notes UpdateNote(Notes objectNote, int[] selectedIdTags);

        /// <summary>
        /// Метод удаления заметки
        /// </summary>
        /// <param name="id">Идентификатор</param>
        void DeleteNote(int id);
        #endregion

        #region Reminder

        /// <summary>
        /// Полечение списка напоминаний
        /// </summary>
        public List<Reminders> GetDataReminder();

        /// <summary>
        /// Получение id напоминания
        /// </summary>
        /// <param name="id">Идентификатор</param>
        public Reminders GetIdReminder(int id);

        /// <summary>
        /// Создание напоминания
        /// </summary>
        /// <param name="objectReminder">Модель напоминаний</param>
        /// <returns></returns>
        public Reminders CreateReminder(Reminders objectReminder);

        /// <summary>
        /// Обнавление существующего напоминания
        /// </summary>
        /// <param name="objectReminder">Модель напоминаний</param>
        public Reminders UpdateReminder(Reminders objectReminder);

        /// <summary>
        /// Удаление напоминания
        /// </summary>
        /// <param name="id">Идентификатор</param>
        void DeleteReminder(int id);

        /// <summary>
        /// Обновление статуса напоминания
        /// </summary>
        /// <param name="changerReminder">Изменёное напоминание</param>
        void StatusValueChangerReminder(Reminders changerReminder);
        #endregion
    }
}
