﻿using Microsoft.EntityFrameworkCore;
using Note.Models;

namespace Note.Data.Interface
{
    /// <summary>
    /// Сервис для работы с заметками
    /// </summary>
    public class NoteServise : INoteService
    {
        #region Конструкторы

        /// <summary>
        /// Сервис для работы с заметками
        /// </summary>
        /// <param name="context">Контекст базы данных</param>
        public NoteServise(ApplicationDBContext context)
        {
            _context = context;
        }
        #endregion

        #region Поля и свойства

        /// <summary>
        /// Контекст базы данных
        /// </summary>
        private readonly ApplicationDBContext _context;
        #endregion


        #region Методы

        /// <summary>
        /// Создание нового тэга
        /// </summary>
        /// <param name="objectTag">Модель тега</param>
        public Tags CreateTag(Tags objectTag)
        {
            _context.Tags.Add(objectTag);
            _context.SaveChanges();
            return GetIdTag(objectTag.Id);
        }

        /// <summary>
        /// Обновление существующего тэга
        /// </summary>
        /// <param name="objectTag">Модель тега</param>
        public Tags UpdateTag(Tags objectTag)
        {
            _context.Tags.Update(objectTag);
            _context.SaveChanges();
            return GetIdTag(objectTag.Id);
        }

        /// <summary>
        /// Получение id тэга
        /// </summary>
        /// <param name="id">Идентификатор</param>
        public Tags GetIdTag(int id)
        {
            return _context.Tags.FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Полечение списка тэгов
        /// </summary>
        public List<Tags> GetDataTags()
        {
            return _context.Tags.ToList();
        }

        /// <summary>
        /// Удаление тега
        /// </summary>
        /// <param name="id">Идентификатор</param>
        public void DeleteTag(int id)
        {
            var element = _context.Tags.FirstOrDefault(x => x.Id == id);
            _context.Tags.Remove(element);
            _context.SaveChanges();
        }

        /// <summary>
        /// Получение списка заметок
        /// </summary>
        public List<Notes> GetDataNotes()
        {
            return _context.Notes.Include(n => n.Tags).ThenInclude(t => t.Notes).ToList();
        }

        /// <summary>
        /// Получение id заметки
        /// </summary>
        /// <param name="id">Идентификатор</param>
        public Notes GetIdNote(int id)
        {
            return _context.Notes.Include(n => n.Tags).FirstOrDefault(n => n.Id == id);
        }

        /// <summary>
        /// Добавление заметки
        /// </summary>
        /// <param name="selectedIdTags">Выбранный идентификатор заметки</param>
        public Notes CreateNote(Notes objectNote, int[] selectedIdTags)
        {
            _context.Notes.Add(objectNote);
            _context.SaveChanges();
            _context.Notes.Include(n => n.Tags).ThenInclude(t => t.Notes).ToList();
            var note = _context.Notes.Find(objectNote.Id);
            foreach (var option in selectedIdTags)
            {
                var tag = _context.Tags.Find(option);
                tag.Notes.Add(note);
                note.Tags.Add(tag);
            }
            _context.SaveChanges();
            return GetIdNote(objectNote.Id);
        }

        /// <summary>
        /// Обновление существующей заметки
        /// </summary>
        /// <param name="objectNote">Модель заметки</param>
        /// <param name="selectedIdTags">Выбранный идентификатор заметки</param>
        public Notes UpdateNote(Notes objectNote, int[] selectedIdTags)
        {
            var note = _context.Notes.Find(objectNote.Id);
            var el = _context.Notes.Include(n => n.Tags).FirstOrDefault(n => n.Id == objectNote.Id);
            foreach (var option in objectNote.Tags)
            {
                var tag = _context.Tags.Find(option.Id);
                tag.Notes.Remove(note);

            }
            _context.SaveChanges();
            foreach (var option in selectedIdTags)
            {
                var tag = _context.Tags.Find(option);
                tag.Notes.Add(note);
                note.Tags.Add(tag);
            }
            _context.Notes.Update(objectNote);
            _context.SaveChanges();
            return GetIdNote(objectNote.Id);
        }

        /// <summary>
        /// Удаление заметки
        /// </summary>
        /// <param name="id">Идентификатор</param>
        public void DeleteNote(int id)
        {
            var element = _context.Notes.Include(n => n.Tags).FirstOrDefault(n => n.Id == id);
            _context.Notes.Remove(element);
            _context.SaveChanges();
        }

        /// <summary>
        /// Полечение списка напоминаний
        /// </summary>
        public List<Reminders> GetDataReminder()
        {
            return _context.Reminders.ToList();
        }

        /// <summary>
        /// Получение id напоминания
        /// </summary>
        /// <param name="id">Идентификатор</param>
        public Reminders GetIdReminder(int id)
        {
            return _context.Reminders.FirstOrDefault(x => x.Id == id);
        }

        /// <summary>
        /// Создание напоминания
        /// </summary>
        /// <param name="objectReminder">Модель напоминаний</param>
        public Reminders CreateReminder(Reminders objectReminder)
        {
            _context.Reminders.Add(objectReminder);
            _context.SaveChanges();
            return GetIdReminder(objectReminder.Id);
        }

        /// <summary>
        /// Обновление конкретного напоминания
        /// </summary>
        /// <param name="objectReminder">Модель напоминаний</param>
        public Reminders UpdateReminder(Reminders objectReminder)
        {
            _context.Reminders.Update(objectReminder);
            _context.SaveChanges();
            return GetIdReminder(objectReminder.Id);
        }

        /// <summary>
        /// Удаление напоминания
        /// </summary>
        /// <param name="id">Идентификатор</param>
        public void DeleteReminder(int id)
        {
            var element = _context.Reminders.FirstOrDefault(x => x.Id == id);
            _context.Reminders.Remove(element);
            _context.SaveChanges();
        }

        /// <summary>
        /// Обновление статуса напоминания
        /// </summary>
        /// <param name="changerReminder">Изменение напоминания</param>
        public void StatusValueChangerReminder(Reminders changerReminder)
        {
            // var item = _context.Reminders.Update(changerReminder);
            var item = _context.Reminders.Attach(changerReminder);
            item.State = EntityState.Modified;
            _context.SaveChanges();
        }
        #endregion
    }
}