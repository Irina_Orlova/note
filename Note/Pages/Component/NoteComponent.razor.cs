﻿using Note.Models;
using Note.Data.Interface;
using Microsoft.AspNetCore.Components;

namespace Note.Pages.Component
{
    /// <summary>
    /// Код компонента "Заметка"
    /// </summary>
    public partial class NoteComponent
    {
        #region Inject

        /// <summary>
        /// Сервис для работы с заметками
        /// </summary>
        [Inject]
        public INoteService NoteService { get; set; } = null!;
        #endregion

        #region Поля и свойств

        /// <summary>
        /// Объект заметки
        /// </summary>
        Notes objectNote = new Notes();

        /// <summary>
        /// Объекты заметок
        /// </summary>
        List<Notes> objectNotes = new List<Notes>();

        /// <summary>
        /// Объекты тегов
        /// </summary>
        List<Tags> objectTags = new List<Tags>();

        /// <summary>
        /// Текстовый массив выбранных тегов при дабовлении в заметку
        /// </summary>
        public string[]? selectedStringIdTags { get; set; }

        /// <summary>
        /// Целочисленный массив выбранных тегов при дабовлении в заметку
        /// </summary>
        public int[]? selectedIdTags { get; set; }
        #endregion

        #region Обработчики событий

        /// <summary>
        /// Инициализация
        /// </summary>
        protected override void OnInitialized()
        {
            objectNotes = new List<Notes>();
            objectNotes = NoteService.GetDataNotes();
            objectTags = NoteService.GetDataTags();
        }
        #endregion

        #region Методы

        /// <summary>
        /// Добавление или обновление заметки
        /// </summary>
        private void AddOrUpdateNote()
        {
            if (objectNote.Id == 0)
            {
                if (!string.IsNullOrWhiteSpace(objectNote.Title))
                {
                    objectNote.DateCreate = DateTime.Now;
                    objectNote = NoteService.CreateNote(objectNote, selectedIdTags);
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(objectNote.Title))
                {
                    objectNote = NoteService.UpdateNote(objectNote, selectedIdTags);
                }
            }

            objectNote = new Notes();

            OnInitialized();
        }

        /// <summary>
        /// ВЫбранная тег
        /// </summary>
        /// <param name="e">Константа</param>
        private void SelectedTagsChanged(ChangeEventArgs e)
        {
            selectedStringIdTags = e.Value as string[];
            selectedIdTags = selectedStringIdTags.Select(x => Int32.Parse(x)).ToArray();  
        }

        /// <summary>
        /// Удаление заметки
        /// </summary>
        /// <param name="id">Идентификатор</param>
        private void DeleteNote(int id)
        {
            NoteService.DeleteNote(id);
            OnInitialized();
        }

        /// <summary>
        /// Редактирование заметки
        /// </summary>
        /// <param name="note">Заметка</param>
        private void EditNoteValue(Notes note)
        {
            objectNote = note;
        }
        #endregion
    }
}
