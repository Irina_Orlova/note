﻿using Note.Models;
using Note.Data.Interface;
using Microsoft.AspNetCore.Components;

namespace Note.Pages.Component
{
    /// <summary>
    /// Код компонента "Напоминание"
    /// </summary>
    public partial class ReminderComponent
    {
        #region Inject

        /// <summary>
        /// Сервис для работы с заметками
        /// </summary>
        [Inject]
        public INoteService NoteService { get; set; } = null!;
        #endregion

        #region Поля и свойств

        /// <summary>
        /// Объект Напоминание
        /// </summary>
        Reminders ObjectReminder = new Reminders();

        /// <summary>
        /// Объекты напоминаий
        /// </summary>
        List<Reminders> ObjectReminders = new List<Reminders>();

        #endregion

        #region Обработчики событий

        /// <summary>
        /// Инициализация напоминаний
        /// </summary>
        protected override void OnInitialized()
        {
            ObjectReminders = new List<Reminders>();
            ObjectReminders = NoteService.GetDataReminder();
        }
        #endregion

        #region Методы
        /// <summary>
        /// Получение идентификаора напоминаний
        /// </summary>
        /// <param name="id">Идентификатор</param>
        private void GetIdReminder(int id)
        {
            ObjectReminder = NoteService.GetIdReminder(id);
        }

        /// <summary>
        /// Добавление или обновление напоминаний
        /// </summary>
        private void AddOrUpdateReminder()
        {
            if (ObjectReminder.Id == 0)
            {
                if (!string.IsNullOrWhiteSpace(ObjectReminder.Name))
                {
                    ObjectReminder.DateCreate = DateTime.Now;
                    ObjectReminder = NoteService.CreateReminder(ObjectReminder);
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(ObjectReminder.Name))
                {
                    ObjectReminder = NoteService.UpdateReminder(ObjectReminder);
                }
            }
            ObjectReminder = new Reminders();
            OnInitialized();
        }

        /// <summary>
        /// Радктирование напоминаний
        /// </summary>
        /// <param name="reminders">Напоминания</param>
        private void EditReminderValue(Reminders reminders)
        {
            ObjectReminder = reminders;
        }

        /// <summary>
        /// Удаление напоминания
        /// </summary>
        /// <param name="id">Идентификатор</param>
        private void DeleteReminder(int id)
        {
            NoteService.DeleteReminder(id);
            OnInitialized();
        }

        /// <summary>
        /// Изменение статуса напоминания
        /// </summary>
        /// <param name="reminders">Напоминания</param>
        private void StatusChangerReminder(Reminders reminders)
        {
            reminders.IsDone = !reminders.IsDone;
            NoteService.StatusValueChangerReminder(reminders);
        }
        #endregion
    }
}
