﻿using Note.Models;
using Note.Data.Interface;
using Microsoft.AspNetCore.Components;

namespace Note.Pages.Component
{
    /// <summary>
    /// Код компонента "Тэг"
    /// </summary>
    public partial class TagComponent
    {
        #region Inject

        /// <summary>
        /// Сервис для работы с заметками
        /// </summary>
        [Inject]
        public INoteService NoteService { get; set; } = null!;
        #endregion

        #region Поля и свойств

        /// <summary>
        /// Флаг наличия ошибки
        /// </summary>
        public bool _hasError;

        /// <summary>
        /// Текст ошибки
        /// </summary>
        public string? _errorMessage;

        /// <summary>
        /// Объект тег
        /// </summary>
        Tags objectTag = new Tags();

        /// <summary>
        /// Объекты заметок
        /// </summary>
        List<Tags> objectTags = new List<Tags>();

        #endregion

        #region Обработчик событий методов

        /// <summary>
        /// Инициализация данных о полученных тегах
        /// </summary>
        protected override void OnInitialized()
        {
            objectTags = new List<Tags>();
            objectTags = NoteService.GetDataTags();
        }

        /// <summary>
        /// Удаление тэга
        /// </summary>
        /// <param name="id">Идентификатор</param>
        private void DeleteTag(int id)
        {
            NoteService.DeleteTag(id);
            OnInitialized();
        }

        /// <summary>
        /// Добавление или обновление тега
        /// </summary>
        private void AddOrUpdateTag()
        {
            if (objectTag.Id == 0)
            {
                if (!string.IsNullOrWhiteSpace(objectTag.Name))
                {
                    objectTag = NoteService.CreateTag(objectTag);
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(objectTag.Name))
                {
                    objectTag = NoteService.UpdateTag(objectTag);
                }
            }
            objectTag = new Tags();
            OnInitialized();
        }

        /// <summary>
        /// Редактирование тега
        /// </summary>
        /// <param name="tag">Тега</param>
        private void EditTagValue(Tags tag)
        {
            objectTag = tag;
        }
        #endregion
    }
}
