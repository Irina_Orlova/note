﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;

namespace Note.Models
{
    /// <summary>
    /// Модель напоминания
    /// </summary>
    public class Reminders
    {
        #region Поля и свойства

        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        /// <summary>
        /// Текст напоминания
        /// </summary>
        [MaybeNull]
        [Required]
        [Column(TypeName = "varchar(50)")]
        public string Name { get; set; }
        /// <summary>
        /// Дата создание напоминания
        /// </summary>
        public DateTime DateCreate { get; set; }

        /// <summary>
        /// Дата и время уведомления напоминания
        /// </summary>
        public DateTime DateReminder { get; set; }

        /// <summary>
        /// Флаг выполненного напоминания
        /// </summary>
        public bool IsDone { get; set; }

    }
    #endregion
}