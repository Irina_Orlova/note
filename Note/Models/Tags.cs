﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace Note.Models
{
    /// <summary>
    /// Модель теги
    /// </summary>
    public class Tags
    {
        #region Поля и свойства

        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Наименорвание тега
        /// </summary>
        [MaybeNull]
        [Required]
        [Column(TypeName ="varchar(50)")]
        public string? Name { get; set; }

        /// <summary>
        /// Заметки
        /// </summary>
        public List<Notes> Notes { get; set; } = new List<Notes>();
        #endregion
    }
}
