﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace Note.Models
{
    /// <summary>
    /// Модель заметки
    /// </summary>
    public class Notes
    {
        #region Поля и свойства

        /// <summary>
        /// Идентификатор
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Наименование заметки
        /// </summary>
        [Required]
        [MaybeNull]
        [Column(TypeName = "varchar(50)")]
        public string Title { get; set; }

        /// <summary>
        /// Содержимое заметки
        /// </summary>
        [MaybeNull]
        [Column(TypeName = "varchar(150)")]
        public string Content { get; set; }

        /// <summary>
        /// Дата создание заметки
        /// </summary>
        public DateTime DateCreate { get; set; }

        /// <summary>
        /// Теги
        /// </summary>
        public List<Tags> Tags { get; set; } = new List<Tags>();
        #endregion
    }
}

