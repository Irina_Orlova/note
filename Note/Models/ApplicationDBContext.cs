﻿using Microsoft.EntityFrameworkCore;

namespace Note.Models
{
    public class ApplicationDBContext : DbContext
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> dbContextOptions) : base(dbContextOptions)
        {
        }

        /// <summary>
        /// Теги
        /// </summary>
        public DbSet<Tags> Tags { get; set; }
        
        /// <summary>
        /// Заметки
        /// </summary>
        public DbSet<Notes> Notes { get; set; }
        
        /// <summary>
        /// Напоминания
        /// </summary>
        public DbSet<Reminders> Reminders { get; set; }
    }
}
